"pslet g:python3_host_prog = '/data/chuanzhi_zheng/miniconda3/envs/text2event/bin/python'


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" 插件管理
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


set nu
let mapleader=","
"set pyxversion=3


"let g:plug_url_format = 'git@github.com:%s.git'
"let g:plug_url_format = 'https://ghproxy.com/https://git::@github.com/%s.git'

" call plug#begin('~/.config/nvim/plugged')

call plug#begin('~/.config/nvim/plugged')
Plug 'joom/vim-commentary'
Plug 'mhinz/vim-startify'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/nerdtree'
" Plug 'kien/ctrlp.vim'
Plug 'easymotion/vim-easymotion'
"Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'preservim/tagbar'
"Plug 'dense-analysis/ale'
" Plug 'skywind3000/asyncrun.vim'
"Plug 'ycm-core/YouCompleteMe'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}

"/data/chuanzhi_zheng/miniconda3/envs/text2event/bin/python

Plug 'lfv89/vim-interestingwords'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'mileszs/ack.vim'
":::checkhealth provider:::

" snip plug
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
" Search
Plug 'brooth/far.vim'
" Color schema
Plug 'cormacrelf/vim-colors-github'		
" Multiple cursors
Plug 'mg979/vim-visual-multi'
" Plug 'Vimjas/vim-python-pep8-indent'
Plug 'jiangmiao/auto-pairs'
" 进行代码注释的插件 gcc注释一行 gacp注释一段 gc注释选中 gcgc取消注释
" 使用默认安装方式安装
" Plug 'tpope/vim-commentary'

" vimux 在vim中直接向tumx发送命令
Plug 'preservim/vimux'
" vim 的python高亮插件 ===有了这个插件才会有那种红线=============
Plug 'vim-python/python-syntax'
" 精致的顶栏插件
Plug 'mg979/vim-xtabline'
" Plug 'jeetsukumaran/vim-buffergator'
"-------这两个只有nvim能用
" Plug 'kyazdani42/nvim-web-devicons'
" Plug 'romgrk/barbar.nvim'
Plug 'ojroques/vim-oscyank', {'branch': 'main'}
" 用于搜索替换的插件
Plug 'dyng/ctrlsf.vim'

Plug 'phaazon/hop.nvim'

" 配置用于markdown的插件
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'

" 预览markdown
" If you have nodejs and yarn
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }
" 表格模式
Plug 'dhruvasagar/vim-table-mode'
" 设置页面边距
Plug 'junegunn/goyo.vim'
if has('nvim')
  function! UpdateRemotePlugins(...)
    " Needed to refresh runtime files
    let &rtp=&rtp
    UpdateRemotePlugins
  endfunction

  Plug 'gelguy/wilder.nvim', { 'do': function('UpdateRemotePlugins') }
else
  Plug 'gelguy/wilder.nvim'

  " To use Python remote plugin features in Vim, can be skipped
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

" ==============================markdown换行插件===========================
Plug 'dkarter/bullets.vim'
call plug#end()

"call coc#float#close_all(1)
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
" nnoremap <C-f> :NERDTreeFind<CR>
" let g:ctrlp_map = '<c-p>'
" let g:ctrlp_cmd = 'CtrlP'
" Gif config
" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)

" Move to line

" map <Leader>l <Plug>(easymotion-bd-jk)
" nmap <Leader>l <Plug>(easymotion-overwin-line)

" Move to word
" map  <Leader>w <Plug>(easymotion-bd-w)
" nmap <Leader>w <Plug>(easymotion-overwin-w)


nmap <F8> :TagbarToggle<CR>



"nnoremap <F9> :call CompileRunGcc()<cr>

func! CompileRunGcc()
          exec "w"
          if &filetype == 'python'
                  if search("@profile")
                          exec "AsyncRun kernprof -l -v %"
                          exec "copen"
                          exec "wincmd p"
                  elseif search("set_trace()")
                          exec "!python3 %"
                  else
                          exec "AsyncRun -raw python3 %"
                          exec "copen"
                          exec "wincmd p"
                  endif
          endif

endfunc



" python-mode config
" let g:pymode_indent = 0
" let g:pymode_lint = 0
" let g:pymode_lint_message = 0
" let g:pymode_lint_on_write = 0
" let g:pymode_options_colorcolumn = 0






""" = 0






""" ncm2 config
"let g:python3_host_prog="/data/chuanzhi_zheng/miniconda3/envs/text2event/bin/python3"
" Use deoplete.

"coc-config
" ==================== coc.nvim ====================
"


" map <c-y> g
" make error texts have a red colorCocRefresh
nmap <leader>h :highlight CocErrorSign   term=standout ctermfg=4 ctermbg=248 guifg=DarkBlue guibg=Grey
" 'coc-explorer', 进行手动安装
let g:coc_global_extensions = ['coc-vimlsp','coc-jedi','coc-explorer','coc-snippets','coc-leetcode','coc-json','coc-docker'
			\,'coc-sh']

" let g:coc_global_extensions = ['coc-vimlsp','coc-pyright','coc-explorer','coc-snippets','coc-leetcode','coc-json','coc-tabnine'
			\,'coc-docker']
" Set internal encoding of vim, not needed on neovim, since coc.nvim using some
" unicode characters in the file autoload/float.vim
set encoding=utf-8

" TextEdit might fail if hidden is not set.
set hidden


" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=100

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

"set suggest.floatEnable="fasle"



" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" enter键不会自动选中第一个
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<CR>"

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
" inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
" 				\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction



" Highlight the symbol and its references when holding the cursor.
" this is so bad setting
" autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
"xmap <leader>f  <Plug>(coc-format-selected)
"nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" get cursor change 
"Mode Settings
if exists('$TMUX')
    let &t_SI = "\<Esc>Ptmux;\<Esc>\e[5 q\<Esc>\\"
    let &t_EI = "\<Esc>Ptmux;\<Esc>\e[2 q\<Esc>\\"
else
    let &t_SI = "\e[5 q"
    let &t_EI = "\e[2 q"
endif
set pastetoggle=<F11>
"Cursor settings:

"  1 -> blinking block
"  2 -> solid block
"  3 -> blinking underscore
"  4 -> solid underscore
"  5 -> blinking vertical bar
"  6 -> solid vertical bar



"let &t_SI = "\<Esc>]50;CursorShape=1\x7"
"let &t_SR = "\<Esc>]50;CursorShape=2\x7"
"let &t_EI = "\<Esc>]50;CursorShape=0\x7"

"highlight some code
nnoremap <silent> <leader>k :call InterestingWords('n')<cr>
vnoremap <silent> <leader>k :call InterestingWords('v')<cr>
nnoremap <silent> <leader>K :call UncolorAllWords()<cr>


nnoremap <silent> n :call WordNavigation(1)<cr>
nnoremap <silent> N :call WordNavigation(0)<cr>
" let g:interestingWordsTermColors = ['154', '121', '211', '137', '214',  '222'] 

"innoremap <C-t> :call coc#float#close_all([1])
					
" inoremap <expr><C-t> :call coc#float#close_all([1])
			
" set some about delete
nnoremap x "_x
nnoremap X "_X
nnoremap d "_d
nnoremap dd "_dd
nnoremap D "_D
vnoremap d "_d
vnoremap dd "_dd
vnoremap c "bc
" vnoremap cc "0
" nnoremap c "0
" nnoremap cc "0
map <c-q> <ESC>"_caw<ESC>p
" map <C-q> dwa<C-r>0<ESC>
"p<BSp<BSp<BSp<BSp<BS>
" ss2s12s12s2s12s122s12s12
"s" w1w21w12
"" s2s12s12
" " s2s12s12










" 此处用于在tmux中可以正常的使用vim的鼠标
" set ttymouse=xterm2
set mouse=a
let g:is_mouse_enabled = 1
noremap <silent> <Leader>m :call ToggleMouse()<CR>
function ToggleMouse()
	if g:is_mouse_enabled == 1
		echo "Mouse OFF"
		set mouse=
		let g:is_mouse_enabled = 0
	else
		echo "Mouse ON"
		set mouse=a
		let g:is_mouse_enabled = 1
	endif	
endfunction

noreabbrev Ack Ack!
nnoremap <Leader>a :Ack!<Space>
nmap <Leader><Leader>a :Ack!<space>-i<space>
au BufNewFile, BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |
    \ set smarttab	
" stop genrate sawp
set nobackup
set noswapfile
set enc=utf-8
set fencs=utf-8,ucs-bom,shift-jis,gb18030,gbk,gb2312,cp936
"语言设置
set langmenu=zh_CN.UTF-8
set helplang=cn
" 使回格键（backspace）正常处理indent, eol, start等
set backspace=2
" 允许backspace和光标键跨越行边界
set whichwrap+=<,>,h,l
" ===
" === Python-syntax
" ===
"let g:python_highlight_all = 1
" snip config
" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim

let g:UltiSnipsExpandTrigger="<leader>SD"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-p>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
" ==================================far config=============================================
" cofig for Find
set lazyredraw            " improve scrolling performance when navigating through large results
set regexpengine=1        " use old regexp engine
set ignorecase smartcase  " ignore case only when the pattern contains no capital letters

" shortcut for far.vim find
nnoremap <silent> <c-f>  :Farf  <cr>
vnoremap <silent> <c-f>  :Farf   <cr>
" 在当前窗口的下面打开搜索预览窗口
let g:far#open_in_parent_window=1
let g:far#preview_window_height=14
" shortcut for far.vim replace
"nnoremap <silent> <Replace-Shortcut>  :Farr<cr>
"vnoremap <silent> <Replace-Shortcut>  :Farr<cr>
"
"
"
"
" ================================Explore======================================
" explorer configuration
" We bind it to <leader>e here, feel free to change this

" let g:coc_explorer_global_presets = {
" \   '.vim': {
" \     'root-uri': '~/.vim',
" \   },
" \   'cocConfig': {
" \      'root-uri': '~/.config/coc',
" \   },
" \   'tab': {
" \     'position': 'tab',
" \     'quit-on-open': v:true,
" \   },
" \   'tab:$': {
" \     'position': 'tab:$',
" \     'quit-on-open': v:true,
" \   },
" \   'floating': {
" \     'position': 'floating',
" \     'open-action-strategy': 'sourceWindow',
" \   },
" \   'floatingTop': {
" \     'position': 'floating',
" \     'floating-position': 'center-top',
" \     'open-action-strategy': 'sourceWindow',
" \   },
" \   'floatingLeftside': {
" \     'position': 'floating',
" \     'floating-position': 'left-center',
" \     'floating-width': 50,
" \     'open-action-strategy': 'sourceWindow',
" \   },
" \   'floatingRightside': {
" \     'position': 'floating',
" \     'floating-position': 'right-center',
" \     'floating-width': 50,
" \     'open-action-strategy': 'sourceWindow',
" \   },
" \   'simplify': {
" \     'file-child-template': '[selection | clip | 1] [indent][icon | 1] [filename omitCenter 1]'
" \   },
" \   'buffer': {
" \     'sources': [{'name': 'buffer', 'expand': v:true}]
" \   },
" \ }

" Use preset argument to open it
nmap <space>ed <Cmd>CocCommand explorer --preset .vim<CR>
nmap <space>ef <Cmd>CocCommand explorer --preset floating<CR>
nmap <space>ec <Cmd>CocCommand explorer --preset cocConfig<CR>
nmap <space>eb <Cmd>CocCommand explorer --preset buffer<CR>

" List all presets
nmap <space>el <Cmd>CocList explPresets<CR>
nmap tt :CocCommand explorer<CR>

" 这个是用来取消鼠标模式和进入复制模式的
nmap <Leader>p  <Leader>m<Esc>:set paste<Esc>

" function! s:explorer_cur_dir()
"   let node_info = CocAction('runCommand', 'explorer.getNodeInfo', 0)
"   return fnamemodify(node_info['fullpath'], ':h')
" endfunction

" function! s:exec_cur_dir(cmd)
"   let dir = s:explorer_cur_dir()
"   execute 'cd ' . dir
"   execute a:cmd
" endfunction


" function! s:enter_explorer()
"   if &filetype == 'coc-explorer'
"     " statusline
"     setl statusline=coc-explorer
"   endif
" endfunction

" augroup CocExplorerCustom
"   autocmd!
"   autocmd BufEnter * call <SID>enter_explorer()
"   autocmd FileType coc-explorer call <SID>init_explorer()
" augroup END





"======================================================== Theme config====================================================
" in your .vimrc or init.vim
colorscheme github
" if you use airline / lightline
" let g:airline_theme = "seoul256"
let g:lightline = { 'colorscheme': 'github' }
" use a slightly darker background, like GitHub inline code blocks
let g:github_colors_soft = 1
" use the dark theme
set background=light

" more blocky diff markers in signcolumn (e.g. GitGutter)
let g:github_colors_block_diffmark = 0
" call github_colors#togglebg_map('<f5>')
"---------------------------------Theme Config end-----------------------------------------------------------
"
" ==================== vim-visual-multi ====================
"let g:VM_theme             = 'iceblue'
let g:VM_default_mappings = 0
"map <Esc>j j
let g:VM_leader                     = {'default': ',', 'visual': ',', 'buffer': ','}
let g:VM_maps                       = {}
" let g:VM_custom_motions             = {'n': 'h', 'i': 'l', 'u': 'k', 'e': 'j', 'N': '0', 'I': '$', 'h': 'e'}
"let g:VM_maps['i']                  = 'k'
"let g:VM_maps['I']                  = 'K'
let g:VM_maps['Find Under']         = '<C-j>'
let g:VM_maps['Find Subword Under'] = '<C-j>'

"let g:VM_maps['Find Next']          = ''
"let g:VM_maps['Find Prev']          = ''
" let g:VM_maps['Remove Region']      = '<Esc>'
let g:VM_maps['Skip Region']        = '<c-n>'
let g:VM_maps["Undo"]               = 'u'
let g:VM_maps["Redo"]               = '<C-r>'
let g:VM_quit_after_leaving_insert_mode = 1
"===================== python indent====================
let g:python_pep8_indent_hang_closing = 1



"==================== vimux============================




let g:VimuxHeight = "50"
let g:VimuxOrientation = "h"
let g:VimuxOpenExtraArgs = ""
let g:VimuxUseNearest = 0
let g:VimuxCloseOnExit = 1
let g:VimuxPromptString = "enter command for tmux splitted pane: "
let g:VimuxRunnerName = "vscode-code-runner"


" just like vs code runner

function! s:code_runner(below)
	if a:below == 1
		let g:VimuxOrientation = "v"
		let g:VimuxOpenExtraArgs = ""
	endif

	if &modified == 1
		w!
	endif 

	if &filetype == "python"
		call VimuxRunCommand("conda activate ".$CONDA_DEFAULT_ENV)
		" 进入当前工作目录
		" call VimuxRunCommand("python3 " . getcwd()  ."/". expand('%:f'))
		" call VimuxRunCommand("python3 " . expand('%:f'))
		call VimuxRunCommand("cd " . expand('%:p:h'))
		call VimuxRunCommand("python3 " . expand('%:p'))
	elseif &filetype == "javascript"
		call VimuxRunCommand("node " . bufname("%"))

	elseif &filetype == "go"
		call VimuxRunCommand("go run " . bufname("%"))

	elseif &filetype == "cpp"
		let filename = split(bufname("%"), "\\V.")[0]
		call VimuxRunCommand("g++ " . bufname("%") . " -o " . filename . " && ./" . filename)

	elseif &filetype == "c"
		let filename = split(bufname("%"), "\\V.")[0]
		call VimuxRunCommand("gcc " . bufname("%") . " -o " . filename . " && ./" . filename)
	
	elseif &filetype == "html"
		let g:VimuxOrientation = "v"
		let g:VimuxOpenExtraArgs = ""
		let g:VimuxHeight = "7"
	
		call VimuxRunCommand("live-server " . bufname("%"))
	endif

	let g:VimuxHeight = "50"
	let g:VimuxOrientation = "h"
	let g:VimuxOpenExtraArgs = ""
endfunction





" ============================配置leetcode============================

function! s:leetcode_runner(below)
	" if a:below == 1
	" 	let g:VimuxOrientation = "v"
	" 	let g:VimuxOpenExtraArgs = ""
	" endif

	let g:VimuxOrientation = "v"
	let g:VimuxOpenExtraArgs = ""

	if &modified == 1
		w!
	endif 

	if &filetype == "python"
		if a:below==1
			call VimuxRunCommand("leetcode test " . bufname("%"))
		else
			call VimuxRunCommand("leetcode submit " . bufname("%"))
		endif
	endif

	let g:VimuxHeight = "50"
	let g:VimuxOrientation = "h"
	let g:VimuxOpenExtraArgs = ""
endfunction

function! s:leetcode_select()
  let g:VimuxHeight = "50"
  let g:VimuxOrientation = "h"
  let g:VimuxOpenExtraArgs = "-b"
  let curline = getline('.')
  call inputsave()
  let name = input('清输入leetcode题号: ')
  call inputrestore()
  call VimuxRunCommand("leetcode show ".name." -g -l python3")
endfunction

nnoremap <silent> <F4> :call <SID>leetcode_runner(1)<CR>
nnoremap <silent> <leader><F4> :call <SID>leetcode_runner(0)<CR>
nnoremap <silent> <F5> :call <SID>leetcode_select()<CR>
" =============================配置leetcode===========================

" code runner orientation to the left side
" nnoremapn
nnoremap <silent> <C-l> :call <SID>code_runner(0)<CR>
" inoremap <silent> <C-A-n> <C-o>:call <SID>code_runner(0)<CR>


" code runner orientation below
nnoremap <silent> <F9> :call <SID>code_runner(1)<CR>
" inoremap <silent> <C-A-b> <C-o>:call <SID>code_runner(1)<CR>


function! s:pytest_compilation()
	if &modified == 1
		w!
	endif 
	if &filetype == "python"
		call VimuxRunCommand("pytest -vv " . getcwd() . "/" . expand('%:f'))
	else
		echo "wrong filetype"
	endif

endfunction

nnoremap <silent> <C-A-m> :call <SID>pytest_compilation()<CR>
inoremap <silent> <C-A-m> <C-o>:call <SID>pytest_compilation()<CR>

nnoremap <silent> <Leader><F9> :VimuxClearTerminalScreen<CR>
inoremap <silent> <C-A-c> <C-o>:VimuxClearTerminalScreen<CR>
 " Close vim tmux runner opened by VimuxRunCommand
map <F10> :VimuxCloseRunner<CR>
"=====================vim 的python高亮===============================
let g:python_highlight_all = 1
let g:python_highlight_indent_errors=0
let g:python_highlight_space_errors=0
" 设置中文编码
let &termencoding=&encoding
set fileencodings=utf-8,gbk,ucs-bom,cp936
" =======================airline的配置===================================
" 把这几句配置加到函数外面任意地方：
" @airline
" set t_Co=256      "在windows中用xshell连接打开vim可以显示色彩
let g:airline#extensions#tabline#enabled = 1   " 是否打开tabline
"这个是安装字体后 必须设置此项" 
let g:airline_powerline_fonts = 1
set laststatus=2  "永远显示状态栏
let g:airline_theme='google_light' "选择主题
" --------------------------------
" let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_tab_nr = 1
let g:airline#extensions#tabline#show_tab_type = 1
"  -------------------------------------

let g:airline#extensions#tabline#show_close_button = 1
let g:airline#extensions#tabline#close_symbol = 'X'
let g:airline#extensions#tabline#enabled=1    "Smarter tab line: 显示窗口tab和buffer
let g:airline#extensions#tabline#left_sep = ' '  "separater
let g:airline#extensions#tabline#left_alt_sep = '|'  "separater
let g:airline#extensions#tabline#formatter = 'default'  "formater
let g:airline_left_sep = '▶'
let g:airline_left_alt_sep = '❯'
let g:airline_right_sep = '◀'
let g:airline_right_alt_sep = '❮'
set t_Co=256
" if exists('+termguicolors')
"     let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"     let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
"     set termguicolors
" endif
" ============================xtabline=======================================
let g:xtabline_settings = {}
let g:xtabline_settings.enabled = 1
let g:xtabline_settings.enable_mappings = 1
let g:xtabline_settings.tabline_modes = ['buffers','tabs']
let g:xtabline_settings.enable_persistance = 0
let g:xtabline_settings.last_open_first = 0
noremap to :XTabCycleMode<CR>
noremap \t :echo expand('%:p')<CR>
"不要用c-e还是用=号
nmap <silent><expr> = v:count ? "\<Plug>(XT-Select-Buffer)" : ":Buffers\r"
"let g:xtabline_lazy = 1
let g:xtabline_settings.buffer_filtering = 1
let g:xtabline_settings.tab_number_in_buffers_mode = 1
" 在这里配置关闭所有其他buffer只保留当前buffer
nnoremap <leader>ca :w <bar> %bd <bar> e# <bar> bd# <CR>

" ==============================按照类型进行缩进==================================
filetype plugin indent on " enable file type detection
" ====================================配置FZF========================================
nnoremap <c-p> :FZF<cr>
" ====================================选中模式下的粘贴复制========================
map <C-v> cw<C-r>0<ESC>
" ====================================auto pairsd防止alt-p等键位=====================
let g:AutoPairsShortcutToggle=""
let g:AutoPairsShortcutJump=""
" ====================================设置换行的问题==============================
" 取消自动换行
" 把textwidth调大：
set textwidth=1000

" 停止在单词中间换行
set linebreak
" 取消自动折行
" set nowrap
set wrap
" ====================================映射向上翻页============================
map <c-l> zz6<c-e>
" =================================== -可以去行末尾===========
map - $
" ========================================进行json格式化=====================
map <leader>js <esc>:%!python -m json.tool<cr>
" ==============================将vim的内容复制到本地======================
vnoremap <leader>c :OSCYank<CR>
let g:oscyank_term = 'tmux'  " or 'screen', 'kitty', 'default'
" 调整可以一次复制的最多字符
let g:oscyank_max_length = 1000000
" =============================CtrlSF===================================
nmap     <C-f> <Plug>CtrlSFPrompt
vmap     <C-f> <Plug>CtrlSFVwordPath
vmap     <C-f> <Plug>CtrlSFVwordExec
nmap     <C-f>n <Plug>CtrlSFCwordPath
nmap     <C-f>p <Plug>CtrlSFPwordPath
nnoremap <C-f>o :CtrlSFOpen<CR>
nnoremap <C-f>t :CtrlSFToggle<CR>
inoremap <C-f>t <Esc>:CtrlSFToggle<CR>
let g:ctrlsf_auto_close = {
    \ "normal" : 0,
    \ "compact": 0
    \}
let g:ctrlsf_auto_focus = {
    \ "at": "start"
    \ }

" 是否自动打开预览窗口
let g:ctrlsf_auto_preview = 0
" 默认打开紧凑视图
let g:ctrlsf_default_view_mode = 'compact'
" 从项目根目录开始搜索
let g:ctrlsf_default_root = 'project'


" ===================加载hop.nvim==================
" 加载 lua/basic.lua 文件，此行为注释
lua require'hop'.setup()
map <leader>w <cmd>HopWord<cr>
map <leader>l <cmd>HopLine<cr>
" =================配置上下移动================
" ==================配置markdown预览==============
" disable header folding
let g:vim_markdown_folding_disabled = 1

" do not use conceal feature, the implementation is not so good
let g:vim_markdown_conceal = 0

" disable math tex conceal feature
let g:tex_conceal = ""
let g:vim_markdown_math = 1

let g:mkdp_open_to_the_world = 1
let g:mkdp_open_ip = '121.4.54.72' " change to you vps or vm ip
" let g:mkdp_port = 8088
let g:mkdp_port = 8391
function! g:EchoUrl(url)
    :echo a:url
endfunction
let g:mkdp_browserfunc = 'g:EchoUrl'
" =============================保存上次的位置==================
" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" ==========================搜索补全======================
" Key bindings can be changed, see below
call wilder#setup({'modes': [':', '/', '?']})

let s:scale = ['#f4468f', '#fd4a85', '#ff507a', '#ff566f', '#ff5e63',
      \ '#ff6658', '#ff704e', '#ff7a45', '#ff843d', '#ff9036',
      \ '#f89b31', '#efa72f', '#e6b32e', '#dcbe30', '#d2c934',
      \ '#c8d43a', '#bfde43', '#b6e84e', '#aff05b']
let s:gradient = map(s:scale, {i, fg -> wilder#make_hl(
      \ 'WilderGradient' . i, 'Pmenu', [{}, {}, {'foreground': fg}]
      \ )})
call wilder#set_option('pipeline', [
      \   wilder#branch(
      \     wilder#cmdline_pipeline({
      \       'fuzzy': 1,
      \       'set_pcre2_pattern': 1,
      \     }),
      \     wilder#python_search_pipeline({
      \       'pattern': 'fuzzy',
      \     }),
      \   ),
      \ ])

let s:highlighters = [
        \ wilder#pcre2_highlighter(),
        \ wilder#pcre2_highlighter(),
        \ ]
" call wilder#set_option('renderer', wilder#popupmenu_renderer(wilder#popupmenu_palette_theme({
"       \ 'border': 'rounded',
"       \ 'max_height': '75%',
"       \ 'min_height': 0,
"       \ 'prompt_position': 'top',
"       \ 'reverse': 0,
"       \ })))

call wilder#set_option('renderer', wilder#renderer_mux({
      \ ':': wilder#popupmenu_renderer({
      \   'gradient': s:gradient,
      \ }),
      \ '/': wilder#popupmenu_renderer({
      \   'gradient': s:gradient,
      \ }),
      \ }))
" ===================================================配置上下移动快捷键===============================
" U/E keys for 5 times u/e (faster navigation)
noremap <silent> U 5k
noremap <silent> E 5j
" Ctrl + U or E will move up/down the view port without moving the cursor
noremap <C-U> 5<C-y>
noremap <C-E> 5<C-e>
" ==========================markdown换行配置  ballet===================
" Bullets.vim
let g:bullets_enabled_file_types = [
    \ 'markdown',
    \ 'text',
    \ 'gitcommit',
    \ 'scratch'
    \]
au BufRead,BufNewFile *.md setlocal wrap
" set numberwidth=5
" set columns=40
" ---------------------------------------
" set columns=30
" autocmd VimResized * if (&columns > 80) | set columns=80 | endif
" set wrap
" set linebreak
" set showbreak=+++
